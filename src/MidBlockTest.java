import static org.junit.Assert.*;

import org.junit.Test;

public class MidBlockTest {
	MidBlock testMidBlock;

	@Test
	public void testMidBlock() {
		//Given:
		testMidBlock = new MidBlock();
		//When:
		//Then:
		assertNotNull(testMidBlock);
		assertEquals(1,testMidBlock.color.length);
	}

	@Test
	public void testMidBlockColors() {
		//Given:
		testMidBlock = new MidBlock(Colors.White);
		//When:
		//Then:
		assertNotNull(testMidBlock);
		assertEquals(1,testMidBlock.color.length);
		assertEquals(Colors.White, testMidBlock.color[0]);
	}

	@Test
	public void testMidBlockColorsArray() {
		//Given:
		Colors[] color = new Colors[1];
		color[0]=Colors.White;
		testMidBlock = new MidBlock(color);
		//When:
		//Then:
		assertNotNull(testMidBlock);
		assertEquals(1,testMidBlock.color.length);
		assertEquals(Colors.White, testMidBlock.color[0]);
		
		//Given:
		Colors[] color2 = new Colors[5];
		color2[0]=Colors.White;
		testMidBlock = new MidBlock(color2);
		//When:
		//Then:
		assertNotNull(testMidBlock);
		assertEquals(1, testMidBlock.color.length);
		assertEquals(color2[0],testMidBlock.color[0]);
	}

	@Test
	public void testGetType() {
		//Given:
		testMidBlock = new MidBlock();
		//When:
		//Then:
		assertEquals("MidBlock", testMidBlock.getType());
	}

	@Test
	public void testGetColorInt() {
		//Given:
		testMidBlock = new MidBlock(Colors.White);
		//When:
		//Then:
		assertEquals(Colors.White, testMidBlock.getColor(0));
	}

	@Test
	public void testGetColor() {
		//Given:
		testMidBlock = new MidBlock(Colors.White);
		Colors[] color = new Colors[1];
		color[0]=Colors.White;
		//When:
		//Then:
		assertArrayEquals(color, testMidBlock.getColor());
	}
	
	@Test
	public void testGetNumberOfColor() {
		//Given:
		testMidBlock=new MidBlock();
		//When:
		//Then:
		assertEquals(1, testMidBlock.getNumberOfColor());
	}

}
