public abstract class Block {
	/**
	 * Tablica kolorow
	 */
	protected Colors[] color;

	/**
	 * Konstruktor domyslny
	 */
	public Block() {

	}

	/**
	 * Konstruktor z przekazaną w parametrze tablica kolorow
	 * 
	 * @param color
	 */
	public Block(Colors[] color) {
		this.color = color;
	}

	/**
	 * 
	 * @return Nazwa Klasy
	 */
	public String getType() {
		return this.getClass().getSimpleName();
	}

	/**
	 * 
	 * @return Ilosc kolorow w bloku
	 */
	public abstract int getNumberOfColor();

	/**
	 * 
	 * @param numberOfWall
	 *            numer sciany
	 * @return Kolor dla danego numeru
	 */
	public Colors getColor(int numberOfWall) {
		return color[numberOfWall];
	}

	/**
	 * 
	 * @return Zwraca wszystkie kolory
	 */
	public Colors[] getColor() {
		return color;
	}

}
