import static org.junit.Assert.*;

import org.junit.Test;


public class CornerBlockTest {
	CornerBlock testCornerBlock;


	@Test
	public void testCornerBlock() {
		//Given:
		testCornerBlock = new CornerBlock();
		//When:
		//Then:
		assertNotNull(testCornerBlock);
		assertEquals(3,testCornerBlock.color.length);
	}

	@Test
	public void testCornerBlockColorsColorsColors() {
		//Given:
		testCornerBlock = new CornerBlock(Colors.White, Colors.Orange, Colors.Blue);
		//When:
		//Then:
		assertNotNull(testCornerBlock);
		assertEquals(3,testCornerBlock.color.length);
		assertEquals(Colors.White, testCornerBlock.color[0]);
		assertEquals(Colors.Orange, testCornerBlock.color[1]);
		assertEquals(Colors.Blue, testCornerBlock.color[2]);
	}

	@Test
	public void testCornerBlockColorsArray() {
		//Given:
		Colors[] color = new Colors[3];
		color[0]=Colors.White;
		color[1]=Colors.Orange;
		color[2]=Colors.Blue;
		testCornerBlock=new CornerBlock(color);
		//When:
		//Then:
		assertNotNull(testCornerBlock);
		assertEquals(3,testCornerBlock.color.length);
		assertEquals(Colors.White, testCornerBlock.color[0]);
		assertEquals(Colors.Orange, testCornerBlock.color[1]);
		assertEquals(Colors.Blue, testCornerBlock.color[2]);
		
		//Given:
		Colors[] color2 = new Colors[6];
		color2[0]=Colors.White;
		color2[1]=Colors.Orange;
		color2[2]=Colors.Blue;
		testCornerBlock=new CornerBlock(color2);
		//When:
		//Then:
		assertNotNull(testCornerBlock);
		assertEquals(3,testCornerBlock.color.length);
		assertEquals(Colors.White, testCornerBlock.color[0]);
		assertEquals(Colors.Orange, testCornerBlock.color[1]);
		assertEquals(Colors.Blue, testCornerBlock.color[2]);
	}

	@Test
	public void testGetType() {
		//Given:
		testCornerBlock=new CornerBlock();
		//When:
		//Then:
		assertEquals("CornerBlock", testCornerBlock.getType());
	}

	@Test
	public void testGetColorInt() {
		//Given:
		testCornerBlock = new CornerBlock(Colors.White, Colors.Orange, Colors.Blue);
		//When:
		//Then:
		assertEquals(Colors.White, testCornerBlock.color[0]);
		assertEquals(Colors.Orange, testCornerBlock.color[1]);
		assertEquals(Colors.Blue, testCornerBlock.color[2]);
	}

	@Test
	public void testGetColor() {
		//Given:
		testCornerBlock = new CornerBlock(Colors.White, Colors.Orange, Colors.Blue);
		Colors[] color = new Colors[3];
		color[0]=Colors.White;
		color[1]=Colors.Orange;
		color[2]=Colors.Blue;
		//When:
		//Then:
		assertArrayEquals(color, testCornerBlock.getColor());
	}

	@Test
	public void testGetNumberOfColor() {
		//Given:
		testCornerBlock = new CornerBlock(Colors.White, Colors.Orange, Colors.Blue);
		//When:
		//Then:
		assertEquals(3, testCornerBlock.getNumberOfColor());
	}
}
