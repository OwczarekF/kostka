import static org.junit.Assert.*;

import org.junit.Test;



public class EdgeBlockTest {
	EdgeBlock testEdgeBlock;


	@Test
	public void testEdgeBlock() {
		//Given:
		testEdgeBlock=new EdgeBlock();
		//When:
		//Then:
		assertNotNull(testEdgeBlock);
		assertEquals(2,testEdgeBlock.color.length);
	}

	@Test
	public void testEdgeBlockColorsColors() {
		//Given:
		testEdgeBlock=new EdgeBlock(Colors.White, Colors.Orange);
		//When:
		//Then:
		assertNotNull(testEdgeBlock);
		assertEquals(2,testEdgeBlock.color.length);
		assertEquals(Colors.White, testEdgeBlock.color[0]);
		assertEquals(Colors.Orange, testEdgeBlock.color[1]);
	}

	@Test
	public void testEdgeBlockColorsArray() {
		//Given:
		Colors[] color = new Colors[2];
		color[0]=Colors.White;
		color[1]=Colors.Orange;
		testEdgeBlock=new EdgeBlock(color);
		//When:
		//Then:
		assertNotNull(testEdgeBlock);
		assertEquals(2,testEdgeBlock.color.length);
		assertEquals(Colors.White, testEdgeBlock.color[0]);
		assertEquals(Colors.Orange, testEdgeBlock.color[1]);
		
		//Given:
		Colors[] color2 = new Colors[6];
		color2[0]=Colors.White;
		color2[1]=Colors.Orange;
		testEdgeBlock=new EdgeBlock(color2);
		//When:
		//Then:
		assertNotNull(testEdgeBlock);
		assertEquals(2, testEdgeBlock.color.length);
		assertEquals(Colors.White, testEdgeBlock.color[0]);
		assertEquals(Colors.Orange, testEdgeBlock.color[1]);
	}

	@Test
	public void testGetType() {
		//Given:
		testEdgeBlock = new EdgeBlock();
		//When:
		//Then:
		assertEquals("EdgeBlock", testEdgeBlock.getType());
	}

	@Test
	public void testGetColorInt() {
		//Given:
		testEdgeBlock=new EdgeBlock(Colors.White, Colors.Orange);
		//When:
		//Then:
		assertEquals(Colors.White, testEdgeBlock.getColor(0));
		assertEquals(Colors.Orange, testEdgeBlock.getColor(1));
	}

	@Test
	public void testGetColor() {
		//Given:
		testEdgeBlock=new EdgeBlock(Colors.White, Colors.Orange);
		Colors[] color = new Colors[2];
		color[0]=Colors.White;
		color[1]=Colors.Orange;
		//When:
		//Then:
		assertArrayEquals(color, testEdgeBlock.getColor());
	}
	
	@Test
	public void testGetNumberOfColor() {
		//Given:
		testEdgeBlock=new EdgeBlock();
		//When:
		//Then:
		assertEquals(2, testEdgeBlock.getNumberOfColor());
	}

}
