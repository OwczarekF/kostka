public class EdgeBlock extends Block {

	public EdgeBlock() {
		super();
		this.color = new Colors[2];
	}

	public EdgeBlock(Colors color0, Colors color1) {
		super();
		this.color = new Colors[2];
		this.color[0] = color0;
		this.color[1] = color1;
	}

	public EdgeBlock(Colors[] color) {
		super(color);
		if (this.color.length > 2){
			this.color = new Colors[2];
			this.color[0]=color[0];
			this.color[1]=color[1];
		}
	}

	@Override
	public int getNumberOfColor() {
		// TODO Auto-generated method stub
		return 2;
	}
}
