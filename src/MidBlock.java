public class MidBlock extends Block {

	public MidBlock() {
		super();
		this.color = new Colors[1];
	}

	public MidBlock(Colors color) {
		this.color = new Colors[1];
		this.color[0] = color;
	}

	public MidBlock(Colors[] color) {
		super(color);
		if (this.color.length > 1){
			this.color = new Colors[1];
			this.color[0]=color[0];
		}
	}

	@Override
	public int getNumberOfColor() {
		return 1;
	}

}
