public class CornerBlock extends Block {

	public CornerBlock() {
		super();
		this.color = new Colors[3];
	}

	public CornerBlock(Colors color0, Colors color1, Colors color2) {
		super();
		this.color = new Colors[3];
		this.color[0] = color0;
		this.color[1] = color1;
		this.color[2] = color2;
	}

	public CornerBlock(Colors[] color) {
		super(color);
		if (this.color.length > 3) {
			this.color = new Colors[3];
			this.color[0] = color[0];
			this.color[1] = color[1];
			this.color[2] = color[2];
		}
	}

	@Override
	public int getNumberOfColor() {
		// TODO Auto-generated method stub
		return 3;
	}

}
